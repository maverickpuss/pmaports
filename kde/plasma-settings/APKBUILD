# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-settings
pkgver=0_git20200829
pkgrel=0
_commit="08fa0c465ba93f6621529897bfaa844f0f58b066"
pkgdesc="Settings application for Plasma Mobile"
arch="all !armhf"
url="https://community.kde.org/Plasma/Mobile"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	kded
	kirigami2
	qt5-qtquickcontrols2
	"
makedepends="
	extra-cmake-modules
	kauth-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdelibs4support-dev
	ki18n-dev
	kio-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	solid-dev
	"
source="https://invent.kde.org/plasma-mobile/plasma-settings/-/archive/$_commit/plasma-settings-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=true ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="074006d91118c3805c6c73d7963496adf8b67fb2560d7acfa717618bc4e9f0a45a1e58c0edb45395c1a8205024089f095cb8f0b53634b83eed089ea3a61c9b05  plasma-settings-08fa0c465ba93f6621529897bfaa844f0f58b066.tar.gz"
