# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-bigscreen
pkgver=0_git20200731
pkgrel=0
_commit="c0497ce2a98db02a62f5e1eb15dcb4761cef2f96"
pkgdesc="A 10-feet interface made for TVs"
url="https://invent.kde.org/plasma/plasma-bigscreen/"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
license="Apache-2.0 AND LGPL-2.0-only AND LGPL-2.1-only AND GPL-2.0-only"
depends="
	kdeconnect
	kirigami2
	plasma-nano
	plasma-nm
	plasma-pa
	plasma-settings
	plasma-workspace
	"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kactivities-stats-dev
	kcmutils-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami2-dev
	knotifications-dev
	kwayland-dev
	kwindowsystem-dev
	plasma-framework-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	"
source="https://invent.kde.org/plasma/plasma-bigscreen/-/archive/$_commit/plasma-bigscreen-$_commit.tar.gz
	plasma-bigscreen.sh
	plasma-bigscreen.desktop
	"
builddir="$srcdir/plasma-bigscreen-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" kcms/audio-device-chooser/CMakeLists.txt
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" kcms/kdeconnect/CMakeLists.txt
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" kcms/wifi/CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install

	install -Dm755 "$srcdir"/plasma-bigscreen.sh \
		"$pkgdir"/usr/bin/plasma-bigscreen

	install -Dm755 "$srcdir"/plasma-bigscreen.desktop \
		"$pkgdir"/usr/share/wayland-sessions/plasma-bigscreen.desktop
}

sha512sums="0a8086cccefb039013f81f539da658d8d86237ff219fe47126ff2efbf9fa5c7d2466a5943bda1ed587e454f0ef986b49c4bf51963a8f9c405cb20ae474b4404e  plasma-bigscreen-c0497ce2a98db02a62f5e1eb15dcb4761cef2f96.tar.gz
dae4247c695fd76d666a605d882817fa0e72d101bc36e15715663c3c980ba01fcb0936761d587f693bb642c957f0ed51b938428ef2d4ee144e8dcf99135049d6  plasma-bigscreen.sh
d7b18b9ebbd1e2f1aad9e30d512c2203d6e610c715494aaa8fef0c63bd52e211b62a639c477073c26309b401af4e9183a5b54ab617eac6f74e58b45f4103d57b  plasma-bigscreen.desktop"
